//
//  Place.swift
//  MyWeather
//
//  Created by Siarhei Lukyanau on 21.11.2021.
//

import Foundation

struct Place {

    var name: String?
    var lat: String?
    var lon: String?
}
