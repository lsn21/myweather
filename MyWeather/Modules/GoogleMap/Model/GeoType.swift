//
//  GeoType.swift
//  MyWeather
//
//  Created by Siarhei Lukyanau on 21.11.2021.
//

import Foundation

enum GeoType {

    case none,
    startingGeo,
    finishingGeo
}
