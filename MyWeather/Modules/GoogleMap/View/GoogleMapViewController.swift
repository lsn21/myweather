//
//  GoogleMapsViewController.swift
//  MyWeather
//
//  Created by Siarhei Lukyanau on 21.11.2021.
//

import UIKit
import GoogleMaps
import GooglePlaces

class GoogleMapViewController: UIViewController, GMSMapViewDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var addressLabel: UITextField!

    var zoom : Float = 16

    var place: Place?

    var locationManager: CLLocationManager?

    var lat: Double = 0
    var lon: Double = 0

    var addressesArray: [GMSAddress]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Выбор места"
        
        mapView.delegate = self
        mapView.setMinZoom(4, maxZoom: 26)

        if Reachability.isConnectedToNetwork() {
            locationManager = CLLocationManager()
            locationManager?.delegate = self
            locationManager?.desiredAccuracy = kCLLocationAccuracyBest
            locationManager?.requestWhenInUseAuthorization()
            locationManager?.startUpdatingLocation()
        }
        else {
            let alert = UIAlertController(title: "Отсутствует подключение к интернет", message: "Невозможно показать карту без подключения к интернет.", preferredStyle: .alert)
            // Создаем кнопку для отмены
            let cancelAction = UIAlertAction(title: "Отмена", style: .destructive, handler: nil)
            
            alert.addAction(cancelAction) // Присваиваем алерут кнопку для отмены
            
            present(alert, animated: true, completion: nil) // Вызываем алёрт контроллер
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        cm.byMap = true
    }
    
    @IBAction func zoomPlusButtonTapped(_ sender: UIButton) {

        if zoom < mapView.maxZoom {

            zoom = zoom + 1
            self.mapView.animate(toZoom: zoom)
        }
    }

    @IBAction func zoomMinusButtonTapped(_ sender: UIButton) {

        if zoom > mapView.minZoom {

            zoom = zoom - 1
            self.mapView.animate(toZoom: zoom)
        }
    }
    
    @IBAction func okButtonTapped(_ sender: Any) {

        var newPlace: Place
        if (addressesArray?.count ?? 0) > 0 {

            // если адрес найден - добавляем адрес
            newPlace = Place(name: (addressesArray?.first?.lines?.joined(separator: ", ")) ?? "", lat: cm.doubleToString(lat), lon: cm.doubleToString(lon))
        }
        else {
            // если адрес найден - добавляем дату и время
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy.MM.dd HH:mm"
            newPlace = Place(name: formatter.string(from: date), lat: cm.doubleToString(lat), lon: cm.doubleToString(lon))
        }
        place = newPlace
        _ = self.tabBarController?.selectedIndex = 0
    }

    func mapView(_ mapView: GMSMapView, idleAt cameraPosition: GMSCameraPosition) {

        reverseGeocodeCoordinate(coordinate: cameraPosition.target)
    }

    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {

        let geocoder = GMSGeocoder()

        // получаем геопозицию
        lat = coordinate.latitude
        lon = coordinate.longitude
        cm.coordinate.lat = lat
        cm.coordinate.lon = lon
        print(cm.coordinate as Any)

        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in

            if (error == nil) {
                
                self.addressesArray = (response?.results())!
                if let address = response?.firstResult() {

                    let lines = address.lines!
                    self.addressLabel.text = lines.joined(separator: ", ")

                    UIView.animate(withDuration: 0.25) {

                        self.view.layoutIfNeeded()
                    }
                } else {

                    self.addressLabel.text = ""
                }
            }
        }
    }
}

extension GoogleMapViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {

        if status == .authorizedWhenInUse {

            locationManager?.startUpdatingLocation()

            mapView.isMyLocationEnabled = true
            mapView.settings.myLocationButton = true
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        if let location = locations.first {

            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: zoom, bearing: 0, viewingAngle: 0)

            // определяем адрес
            reverseGeocodeCoordinate(coordinate: location.coordinate)

            locationManager?.stopUpdatingLocation()
        }
        
    }
}
