//
//  TodayEnum.swift
//  MyWeather
//
//  Created by Siarhei Lukyanau on 19.11.2021.
//

import Foundation

enum TodayEnum {
    case loading
    case success(CurrentWeather)
    case failed(Bool)
}
