//
//  TodayRealmModel.swift
//  MyWeather
//
//  Created by Siarhei Lukyanau on 21.11.2021.
//

import Foundation
import RealmSwift

struct TodayModel {
    var id: Int = 0
    var cityAndCountry = ""
    var temperature = ""
    var icon = ""
    var humidity = ""
    var pressure = ""
    var windSpeed = ""
    var windDirection = ""
    var clouds = ""
    var temp: Double = 0
}

class TodayRealmModel: Object {
    @objc dynamic var id = 0
    @objc dynamic var cityAndCountry = ""
    @objc dynamic var temperature = ""
    @objc dynamic var icon = ""
    @objc dynamic var humidity = ""
    @objc dynamic var pressure = ""
    @objc dynamic var windSpeed = ""
    @objc dynamic var windDirection = ""
    @objc dynamic var clouds = ""
    @objc dynamic var temp: Double = 0

    init(today: TodayModel) {
        self.cityAndCountry = today.cityAndCountry
        self.temperature = today.temperature
        self.icon = today.icon
        self.humidity = today.humidity
        self.pressure = today.pressure
        self.windSpeed = today.windSpeed
        self.windDirection = today.windDirection
        self.clouds = today.clouds
        self.temp = today.temp
    }

    required override init() {
    }

    override static func primaryKey() -> String? {
        return "id"
    }
}
