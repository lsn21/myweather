//
//  CurrentWeatherSys.swift
//  MyWeather
//
//  Created by Siarhei Lukyanau on 19.11.2021.
//

import Foundation

struct CurrentWeatherSys: Codable {
    let country: String?
    let sunrise: Int?
    let sunset: Int?

    static func emptyInit() -> CurrentWeatherSys {
        return CurrentWeatherSys(
            country: "",
            sunrise: 0,
            sunset: 0
        )
    }
}
