//
//  CurrentWeatherMainValue.swift
//  MyWeather
//
//  Created by Siarhei Lukyanau on 19.11.2021.
//

import Foundation

struct CurrentMainValue: Codable {
    
    let temp: Double?
    let feelsLike: Double?
    let tempMin: Double?
    let tempMax: Double?
    let pressure: Int?
    let humidity: Int?

    enum CodingKeys: String, CodingKey {
        case temp
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure
        case humidity
    }
    
    static func emptyInit() -> CurrentMainValue {
        return CurrentMainValue(
            temp: 0.0,
            feelsLike: 0.0,
            tempMin: 0,
            tempMax: 0,
            pressure: 0,
            humidity: 0
        )
    }
}
