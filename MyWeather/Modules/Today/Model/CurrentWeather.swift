//
//  CurrentWeather.swift
//  MyWeather
//
//  Created by Siarhei Lukyanau on 19.11.2021.
//

import Foundation

struct CurrentWeather: Codable {
    let timezone: Int?
    let id: Int?
    let name: String?
    let coordinate: Coordinate?
    let elements: [WeatherElement]?
    let base: String?
    let mainValue: CurrentMainValue?
    let visibility: Int?
    let wind: WeatherWind?
    let clouds: WeatherClouds?
    let date: Int?
    let sys: CurrentWeatherSys?
    let code: Int?
    
    enum CodingKeys: String, CodingKey {
        case timezone
        case id
        case name
        case coordinate = "coord"
        case base
        case mainValue = "main"
        case visibility
        case wind
        case clouds
        case date = "dt"
        case sys
        case elements = "weather"
        case code = "cod"
    }
    
    static func emptyInit() -> CurrentWeather {
        return CurrentWeather(
            timezone: 0,
            id: 0,
            name: "",
            coordinate: Coordinate.emptyInit(),
            elements: [],
            base: "",
            mainValue: CurrentMainValue.emptyInit(),
            visibility: 0,
            wind: WeatherWind.emptyInit(),
            clouds: WeatherClouds.emptyInit(),
            date: 0,
            sys: CurrentWeatherSys.emptyInit(),
            code: 0
        )
    }
    
    func description() -> String {
        var result = "Today: "
        if let weatherElement = elements?.first {
            result += "\((weatherElement.weatherDescription ?? "").capitalizingFirstLetter()) currently. "
        }
        result += "It's \(mainValue?.temp ?? 0)°."
        return result
    }
    
    func getForecastWeather() -> ForecastWeather {
        var result = ForecastWeather.emptyInit()

        result.date = self.date ?? 0
        result.mainValue.tempMin = self.mainValue?.tempMin ?? 0
        result.mainValue.tempMax = self.mainValue?.tempMax ?? 0

        if let weatherElement = elements?.first {
            result.elements.append(weatherElement)
        }

        return result
    }
}
