//
//  TodayViewController.swift
//  MyWeather
//
//  Created by Siarhei Lukyanau on 19.11.2021.
//

import UIKit
import RealmSwift

protocol TodayViewProtocol {
    func setShowOrHideSegmentedControl()
}

class TodayViewController: UIViewController {

    @IBOutlet var cityAndCountryLabel: UILabel!
    @IBOutlet var temperatureLabel: UILabel!
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var humidityLabel: UILabel!
    @IBOutlet var pressureLabel: UILabel!
    @IBOutlet var windSpeedLabel: UILabel!
    @IBOutlet var windDirectionLabel: UILabel!
    @IBOutlet var cloudsLabel: UILabel!
    @IBOutlet var adviceLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var byMapButton: UIButton!
    
    var updateViewData: ((TodayEnum) -> ())?
    let client = APIClient()
    
    var currentWeather = CurrentWeather.emptyInit()
    
    private var stateCurrentWeather = StateEnum.loading

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Today"
        Location.shared.todayDelegate = self
        activityIndicator.color = .orange
        activityIndicator.hidesWhenStopped = true
        byMapButton.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getData()
    }
    
    @IBAction func segmentedControlTapped(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            byMapButton.isHidden = true
            cm.byMap = false
            getData()
        }
        else {
            byMapButton.isHidden = false
            cm.byMap = true
        }
    }
    
    @IBAction func byMapButtonTapped(_ sender: Any) {
        _ = self.tabBarController?.selectedIndex = 1
    }
    
    private func getData() {
        
        activityIndicator.startAnimating()
        if stateCurrentWeather == .loading {
            updateViewData?(.loading)
        }
        var coordinate = Coordinate()
        if cm.status == .denied || cm.byMap {
            print(cm.coordinate as Any)
            coordinate = cm.coordinate
        }
        else {
            coordinate = Location.shared.getLocation()
        }
        client.getCurrentWeather(at: coordinate) { [weak self] currentWeather, error in
            guard let ws = self else { return }
            ws.activityIndicator.stopAnimating()
            if let currentWeather = currentWeather {
                ws.currentWeather = currentWeather
                ws.stateCurrentWeather = .success
            }
            else {
                if Reachability.isConnectedToNetwork(){
                    ws.stateCurrentWeather = .failed
                }
                else{
                    ws.stateCurrentWeather = .noInternet
                }
            }
            ws.updateStateView()
        }
    }
        
    private func updateStateView() {
        if stateCurrentWeather == .success {
            updateViewData?(.success(currentWeather))
        }
        if stateCurrentWeather == .failed {
            updateViewData?(.failed(true))
        }
        if stateCurrentWeather == .noInternet {
            updateViewData?(.failed(false))
        }
        setLabels()
    }

    private func setLabels() {
        let mainValue = currentWeather.mainValue
        let wind = currentWeather.wind
        let clouds = currentWeather.clouds
        
        if Reachability.isConnectedToNetwork() {
            cityAndCountryLabel.text = "\(currentWeather.name ?? ""), \(currentWeather.sys?.country ?? "")"
            temperatureLabel.text = "\(mainValue?.temp ?? 0)°C | \(currentWeather.elements?.first?.main ?? "")"
            iconImageView.image = UIImage(named: currentWeather.elements?.first?.icon ?? "")
            humidityLabel.text = "\(mainValue?.humidity ?? 0)%"
            pressureLabel.text = "\(mainValue?.pressure ?? 0) hPa"
            windSpeedLabel.text = "\(wind?.speed ?? 0) m/s"
            windDirectionLabel.text = windDirection(wind?.deg ?? 0)
            cloudsLabel.text = "\(clouds?.all ?? 0)%"
            
            var today = TodayModel()
            today.id = 0
            today.cityAndCountry = cityAndCountryLabel.text ?? ""
            today.temperature = temperatureLabel.text ?? ""
            today.icon = currentWeather.elements?.first?.icon ?? ""
            today.humidity = humidityLabel.text ?? ""
            today.pressure = pressureLabel.text ?? ""
            today.windSpeed = windSpeedLabel.text ?? ""
            today.windDirection = windDirectionLabel.text ?? ""
            today.clouds = cloudsLabel.text ?? ""
            today.temp = mainValue?.temp ?? 0
            
            let todayRealm = TodayRealmModel.init(today: today)

            do {
                let realm = try Realm()
                try realm.write {
                    realm.add(todayRealm, update: .all)
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(true, forKey: "todaySaved")
                    userDefaults.synchronize()
                }
            }
            catch {
                print("Error Realm")
            }
            advice(mainValue?.temp ?? 0)
        }
        else {
            cityAndCountryLabel.text = ""
            temperatureLabel.text = ""
            iconImageView.image = UIImage(named: "")
            humidityLabel.text = ""
            pressureLabel.text = ""
            windSpeedLabel.text = ""
            windDirectionLabel.text = ""
            cloudsLabel.text = ""
            
            let todaySaved = UserDefaults.standard.bool(forKey: "todaySaved")
            let alert = UIAlertController(title: "Отсутствует подключение к интернет", message: todaySaved ? "Показать данные сохраненные локально?" : "Данные ещё не были сохранены локально", preferredStyle: .alert)
            
            if todaySaved {
                // Создание кнопки для вывода сохраненной информации
                let saveAction = UIAlertAction(title: "Показать", style: .default) { [weak self]  action in
                    guard let ws = self else { return }
                    do {
                        let today = try Realm().objects(TodayRealmModel.self)
                        let todayFirst = today.first
                        ws.cityAndCountryLabel.text = todayFirst?.cityAndCountry
                        ws.temperatureLabel.text = todayFirst?.temperature
                        ws.iconImageView.image = UIImage(named: todayFirst?.icon ?? "")
                        ws.humidityLabel.text = todayFirst?.humidity
                        ws.pressureLabel.text = todayFirst?.pressure
                        ws.windSpeedLabel.text = todayFirst?.windSpeed
                        ws.windDirectionLabel.text = todayFirst?.windDirection
                        ws.cloudsLabel.text = todayFirst?.clouds
                        
                        let temp = todayFirst?.temp ?? 0
                        ws.advice(temp)
                    }
                    catch {}
                }
                alert.addAction(saveAction) // Присваиваем алёрту кнопку для вывода сохраненной информации
            }
            
            // Создаем кнопку для отмены
            let cancelAction = UIAlertAction(title: "Отмена", style: .destructive, handler: nil)
            
            alert.addAction(cancelAction) // Присваиваем алерут кнопку для отмены 
            
            present(alert, animated: true, completion: nil) // Вызываем алёрт контроллер
        }
    }
    
    func windDirection(_ deg: Int) -> String {
        var direction = ""
        
        switch deg {
        case 0...22, 338...360:
            direction = "N"
        case 23...67:
            direction = "NE"
        case 68...112:
            direction = "E"
        case 113...157:
            direction = "SE"
        case 158...202:
            direction = "S"
        case 203...247:
            direction = "SW"
        case 248...292:
            direction = "W"
        case 293...337:
            direction = "NW"
        default:
            direction = ""
        }
        return direction
    }
    
    func advice(_ temp: Double) {
        if temp < 0 {
            adviceLabel.text = "температура меньше 0 градусов"
        }
        else if temp >= 0 && temp < 15 {
            adviceLabel.text = "температура от 0 до 15 градусов"
        }
        else {
            adviceLabel.text = "температура выше 15 градусов"
        }
    }
}

extension TodayViewController: TodayViewProtocol {
    func setShowOrHideSegmentedControl() {
        if cm.status == .denied {
            segmentedControl.isHidden = true
            byMapButton.isHidden = false
        }
        else {
            segmentedControl.isHidden = false
        }
    }
    
}
