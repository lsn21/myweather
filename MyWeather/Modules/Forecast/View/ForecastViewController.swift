//
//  ForecastViewController.swift
//  MyWeather
//
//  Created by Siarhei Lukyanau on 20.11.2021.
//

import UIKit
import RealmSwift

class ForecastViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var viewData: ForecastEnum = .loading {
        didSet {
            view.setNeedsLayout()
        }
    }
    var forecastWeatherByDays: ForecastWeatherByDays?
    
    var updateViewData: ((ForecastEnum) -> ())?
    let client = APIClient()
    
    private var stateForecastWeather = StateEnum.loading
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.sectionHeaderHeight = 24
        tableView.sectionHeaderTopPadding = 0
        activityIndicator.color = .orange
        activityIndicator.hidesWhenStopped = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        getData()
    }

    private func getData() {
        activityIndicator.startAnimating()
        
        var coordinate = Coordinate()
        if cm.status == .denied || cm.byMap {
            coordinate = cm.coordinate
        }
        else {
            coordinate = Location.shared.getLocation()
        }
        if Reachability.isConnectedToNetwork() {
            client.getForecastWeather(at: coordinate) { [weak self] forecastWeatherResponse, error in
                guard let ws = self else { return }
                ws.activityIndicator.stopAnimating()
                if let forecastWeatherResponse = forecastWeatherResponse {
                    ws.forecastWeatherByDays = forecastWeatherResponse.byDaysList
                    ws.stateForecastWeather = .success
                    let city = forecastWeatherResponse.city
                    ws.title = "\(city.name), \(city.country)"
                    
                    let list = forecastWeatherResponse.list
                    var listRealm = [ForecastRealmModel]()

                    for i in 0..<list.count {
                        var item = ForecastModel()
                        let listItem = list[i]
                        let element = listItem.elements.first
                        print(element as Any)
                        item.id = i
                        item.icon = element?.icon ?? ""
                        item.date = listItem.date
                        item.weatherDescription = element?.weatherDescription ?? ""
                        item.temp = listItem.mainValue.temp

                        let realmItem = ForecastRealmModel.init(forecas: item)
                        listRealm.append(realmItem)
                    }
                    do {
                        let realm = try Realm()
                        try realm.write {
                            realm.add(listRealm, update: .all)
                            let userDefaults = UserDefaults.standard
                            userDefaults.set(true, forKey: "forecastSaved")
                            userDefaults.synchronize()
                        }
                    }
                    catch {
                        print("Error Realm")
                    }
                }
                else {
                    if Reachability.isConnectedToNetwork(){
                        ws.stateForecastWeather = .failed
                    }
                    else{
                        ws.stateForecastWeather = .noInternet
                    }
                }
                ws.updateStateView()
            }
        }
        else {
            activityIndicator.stopAnimating()
            let forecastSaved = UserDefaults.standard.bool(forKey: "forecastSaved")
            let alert = UIAlertController(title: "Отсутствует подключение к интернет", message: forecastSaved ? "Показать данные сохраненные локально?" : "Данные ещё не были сохранены локально", preferredStyle: .alert)
            
            if forecastSaved {
                // Создание кнопки для вывода сохраненной информации
                let saveAction = UIAlertAction(title: "Показать", style: .default) { [weak self]  action in
                    guard let ws = self else { return }
                    
                    var result = ForecastWeatherByDays.emptyInit()
                    do {
                        let realm = try Realm()
                        let today = realm.objects(TodayRealmModel.self)
                        let todayFirst = today.first
                        result.city = todayFirst?.cityAndCountry ?? ""
                        ws.title = result.city
                    }
                    catch {
                        print("Error Realm")
                    }
                    var forecastWeatherByDays = ForecastWeatherByDay.emptyInit()

                    forecastWeatherByDays.dayOfWeek = "Today"
                    do {
                        let realm = try Realm()
                        let rForecast = realm.objects(ForecastRealmModel.self)
                        let lForecast = rForecast.reduce(List<ForecastRealmModel>()) { (list, element) -> List<ForecastRealmModel> in
                            list.append(element)
                            return list
                        }
                        let forecast = ws.fromRForecast(rforecast: lForecast)
                        var before = forecast.first
                        print(before as Any)
                        for weather in forecast {
                            print(weather.date.dateFromMilliseconds().dayWord())
                            print(before?.date.dateFromMilliseconds().dayWord() as Any)
                            if weather.date.dateFromMilliseconds().dayWord() == before?.date.dateFromMilliseconds().dayWord() {
                                
                                let weatherItem = ForecastWeatherItem()
                                weatherItem.icon = weather.icon
                                weatherItem.date = weather.date
                                weatherItem.weatherDescription = weather.weatherDescription
                                weatherItem.temp = weather.temp

                                forecastWeatherByDays.forecastWeather.append(weatherItem)
                            }
                            else {
                                result.weatherByDays.append(forecastWeatherByDays)
                                before = weather
                                forecastWeatherByDays = ForecastWeatherByDay.emptyInit()
                                
                                let weatherItem = ForecastWeatherItem()
                                weatherItem.icon = weather.icon
                                weatherItem.date = weather.date
                                weatherItem.weatherDescription = weather.weatherDescription
                                weatherItem.temp = weather.temp
                                
                                forecastWeatherByDays.forecastWeather.append(weatherItem)
                                
                                forecastWeatherByDays.dayOfWeek = weather.date.dateFromMilliseconds().dayWord()
                            }
                        }
                        result.weatherByDays.append(forecastWeatherByDays)
                        ws.forecastWeatherByDays = result
                        ws.tableView.reloadData()
                    }
                    catch {
                        print("Error Realm")
                    }
                }
                alert.addAction(saveAction) // Присваиваем алёрту кнопку для вывода сохраненной информации
            }
            
            // Создаем кнопку для отмены
            let cancelAction = UIAlertAction(title: "Отмена", style: .destructive) { [weak self]  action in
                guard let ws = self else { return }
                
                ws.tableView.reloadData()
            }
            
            alert.addAction(cancelAction) // Присваиваем алерут кнопку для отмены
            
            present(alert, animated: true, completion: nil) // Вызываем алёрт контроллер
        }
    }

    func fromRForecast(rforecast: List<ForecastRealmModel>) -> [ForecastModel] {
        var forecast = [ForecastModel]()
        rforecast.forEach { frm in
            var fm = ForecastModel()

            fm.id = frm.id
            fm.icon = frm.icon
            fm.date = frm.date
            fm.weatherDescription = frm.weatherDescription
            fm.temp = frm.temp

            forecast.append(fm)
        }
        return forecast
    }

    private func updateStateView() {
        if stateForecastWeather == .success {
            updateViewData?(.success(forecastWeatherByDays ?? ForecastWeatherByDays.emptyInit()))
            tableView.reloadData()
        }
        if stateForecastWeather == .failed {
            updateViewData?(.failed(true))
        }
        if stateForecastWeather == .noInternet {
            updateViewData?(.failed(false))
        }
    }
}

extension ForecastViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {

        view.tintColor = .orange
        let header : UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = .white
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let dayOfWeek = forecastWeatherByDays?.weatherByDays[section].dayOfWeek ?? ""
        return dayOfWeek
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        let count = forecastWeatherByDays?.weatherByDays.count ?? 0
        return count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = forecastWeatherByDays?.weatherByDays[section].forecastWeather.count ?? 0
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = indexPath.section
        let row = indexPath.row
        let forecastWeather = forecastWeatherByDays?.weatherByDays[section].forecastWeather[row]
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier:"ForecastCell", for: indexPath) as! ForecastCell
        
        cell.iconImageView.image = UIImage(named: forecastWeather?.icon ?? "")
        cell.timeLabel.text = "\(forecastWeather?.date.dateFromMilliseconds().hourMinute() ?? "")"
        cell.descriptionLabel.text = "\(forecastWeather?.weatherDescription ?? "")"
        cell.temperatureLabel.text = "\(Int(round(forecastWeather?.temp ?? 0)))°C"

        return cell
    }
}

extension ForecastViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
}
