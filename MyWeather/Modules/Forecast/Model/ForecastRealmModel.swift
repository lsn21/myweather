//
//  ForecastRealmModel.swift
//  MyWeather
//
//  Created by Siarhei Lukyanau on 21.11.2021.
//

import Foundation
import RealmSwift

struct ForecastModel {
    var id: Int = 0
    var icon = ""
    var date: Int = 0
    var weatherDescription = ""
    var temp: Double = 0
}

class ForecastRealmModel: Object {
    @objc dynamic var id: Int = 0
    @objc dynamic var icon = ""
    @objc dynamic var date: Int = 0
    @objc dynamic var weatherDescription = ""
    @objc dynamic var temp: Double = 0

    init(forecas: ForecastModel) {
        self.id = forecas.id
        self.icon = forecas.icon
        self.date = forecas.date
        self.weatherDescription = forecas.weatherDescription
        self.temp = forecas.temp
    }

    required override init() {
    }

    override static func primaryKey() -> String? {
        return "id"
    }
}
