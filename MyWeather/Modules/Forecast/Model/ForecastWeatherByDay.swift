//
//  ForecastWeatherByDays.swift
//  MyWeather
//
//  Created by Siarhei Lukyanau on 19.11.2021.
//

import Foundation

class ForecastWeatherItem {
    var icon = ""
    var date: Int = 0
    var weatherDescription = ""
    var temp: Double = 0
}


struct ForecastWeatherByDay {
    
    var dayOfWeek: String
    var forecastWeather: [ForecastWeatherItem]
    
    static func emptyInit() -> ForecastWeatherByDay {
        return ForecastWeatherByDay(
            dayOfWeek: "",
            forecastWeather: []
        )
    }
}

struct ForecastWeatherByDays {
    var city: String
    var weatherByDays: [ForecastWeatherByDay]
        
    static func emptyInit() -> ForecastWeatherByDays {
        return ForecastWeatherByDays(
            city: "",
            weatherByDays: []
        )
    }
}
