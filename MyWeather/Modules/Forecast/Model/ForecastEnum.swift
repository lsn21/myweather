//
//  ForecastViewData.swift
//  MyWeather
//
//  Created by Siarhei Lukyanau on 19.11.2021.
//

import Foundation

enum ForecastEnum {
    case loading
    case success(ForecastWeatherByDays)
    case failed(Bool)
}
