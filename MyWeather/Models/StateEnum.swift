//
//  StateView.swift
//  MyWeather
//
//  Created by Siarhei Lukyanau on 19.11.2021.
//

import Foundation

enum StateEnum {
    case loading
    case success
    case failed
    case noInternet
}
