//
//  WeatherClouds.swift
//  MyWeather
//
//  Created by Siarhei Lukyanau on 19.11.2021.
//

import Foundation

struct WeatherClouds: Codable {
    let all: Int?

    static func emptyInit() -> WeatherClouds {
        return WeatherClouds(all: 0)
    }
}
