//
//  Coordinate.swift
//  MyWeather
//
//  Created by Siarhei Lukyanau on 19.11.2021.
//

import Foundation

struct Coordinate: Codable {
    var lon: Double?
    var lat: Double?

    static func emptyInit() -> Coordinate {
        return Coordinate(lon: 0, lat: 0)
    }
}
