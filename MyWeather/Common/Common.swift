//
//  Common.swift
//  MyWeather
//
//  Created by Siarhei Lukyanau on 21.11.2021.
//

import UIKit
import CoreLocation

let cm = Common.shared

final class Common {

    // Can't init is singleton
    private init() {

    }
    //MARK: Shared Instance
    static let shared: Common = Common()

    // текущий UIViewController для полноэкранной рекламы
    var currentController: UIViewController!
    var accuracy: Int = 2
    
    var coordinate = Coordinate()
    var status: CLAuthorizationStatus?
    var byMap = false

    func getAccuracy() -> CLLocationAccuracy {

        var locationAccuracy: CLLocationAccuracy = kCLLocationAccuracyBest

        switch accuracy {
        case 1:
            locationAccuracy = kCLLocationAccuracyBestForNavigation
            break
        case 2:
            locationAccuracy = kCLLocationAccuracyBest
            break
        case 3:
            locationAccuracy = kCLLocationAccuracyNearestTenMeters
            break
        case 4:
            locationAccuracy = kCLLocationAccuracyHundredMeters
            break
        case 5:
            locationAccuracy = kCLLocationAccuracyKilometer
            break
        case 6:
            locationAccuracy = kCLLocationAccuracyThreeKilometers
            break
        default:
            break
        }
        return locationAccuracy
    }

    // обрезает числовую строку source, signs - количество цифр после запятой, по умолчанию - 6
    func doubleToString(_ source: Double, signs: Int = 6) -> String {
        let s = String(format: "%f", source)
        return s
    }
    
    // обрезает числовую строку source, signs - количество цифр после запятой, по умолчанию - 6
    func truncString(_ source: String, signs: Int = 6) -> String {
        var result = source
        if source.range(of:".") != nil {
            let range = source.range(of:".")
            let index = source.index((range?.upperBound)!, offsetBy: signs)
            //result = source.substring(to: index)
            result = "\(source[..<index])"
        }
        return result
    }
    
    // возвращает символ в позиции i
    func charStr(_ string: String, i: Int) -> String {
        let start = string.index(string.startIndex, offsetBy: i)
        let end = string.index(string.startIndex, offsetBy: i + 1)
        return "\(string[start..<end])"
    }

    // возвращает подстроку с позиции i
    func fromStr(_ string: String, i: Int) -> String {
        let index = string.index(string.startIndex, offsetBy: i)
        return "\(string[index...])"
    }

    // градусы в радианы
    func d2r(_ degrees: Double) -> Double {
        return degrees * .pi / 180.0
    }

    // радианы в градусы
    func radiansToDegrees(_ radians: Double) -> Double {
        return radians * 180.0 / .pi
    }

    // возвращает азимут
    func getAzimuth(fromLoc: CLLocation, toLoc: CLLocation) -> Double {

        let fLat: Double = d2r(fromLoc.coordinate.latitude)
        let fLng: Double = d2r(fromLoc.coordinate.longitude)
        let tLat: Double = d2r(toLoc.coordinate.latitude)
        let tLng: Double = d2r(toLoc.coordinate.longitude)

        let a: Double = (sin(fLng-tLng)*cos(tLat))
        let b: Double = (cos(fLat)*sin(tLat)-sin(fLat)*cos(tLat)*cos(fLng-tLng))

        return atan2(a,b);
    }
    
    // обрабатывает ввод с клавиатуры для дробного цифрового поля
    func digitalField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        var addSymbol = false
        var symbol = string
        let text = textField.text
        let lenght = text?.count

        if (textField.tag > 1) {
            if (string.count == 0) {
                return true;
            }
            if symbol == "," {
                symbol = "."
            }
            let allowedCharacters = CharacterSet.decimalDigits
            let characterSet = CharacterSet(charactersIn: symbol)

            // если цифра
            if allowedCharacters.isSuperset(of: characterSet) {

                addSymbol = true

                if (lenght == 1) && (cm.charStr(text!, i: 0) == "0") && (cm.charStr(text!, i: 1) != ".") {
                    textField.text = cm.fromStr(text!, i: 1)
                }
                if (lenght == 1) && (cm.charStr(text!, i: 0) == "0") && (cm.charStr(symbol, i: 1) == "0") {
                    addSymbol = false
                }
                else {
                    addSymbol = true
                }
            }
                // если другой символ
            else {
                addSymbol = false
            }
            if symbol == "." {

                if !textField.selectedTextRange!.isEmpty || lenght == 0 {
                    textField.text = "0"
                    addSymbol = true
                }
                else if range.location == 1 && cm.charStr(text!, i: 0) == "-" && lenght == 1 {
                    let insertIndex = text?.index((text?.startIndex)!, offsetBy: 1)
                    textField.text?.insert("0", at: insertIndex!)
                    addSymbol = true
                }
                else {
                    let r = text?.range(of:".")
                    if r != nil {
                        addSymbol = false
                    }
                    else {
                        addSymbol = true
                    }
                }
            }
            if symbol == "-" {
                if (!textField.selectedTextRange!.isEmpty || lenght == 0 || range.location == 0) {
                    addSymbol = true
                    let r = text?.range(of:"-")
                    if r != nil {
                        addSymbol = false
                    }
                    else {
                        addSymbol = true
                    }
                }
            }
        }
        else {
            addSymbol = true
        }
        return addSymbol
    }
}
